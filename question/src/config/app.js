const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  mongodb:{  
    dbHost: process.env.DB_HOST,
    dbName: process.env.DB_NAME,
    dbPort: process.env.DB_PORT
  }
};