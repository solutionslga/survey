class QuestionService{

    constructor(questionModel){
        this._questionModel = questionModel; 
    }

    async createQuestion(questions){
        const {questionId} = questions;
        await this._questionModel.updateOne(
            {questionId},
            questions, { upsert: true });
    }
    
    getAllQuestions(){
        return this._questionModel.find();
    }

    getQuestionById(questionId){
        return this._questionModel.find({questionId});
    }

    deleteQuestionById(questionId){
        return this._questionModel.remove({questionId});
    }

}

module.exports = QuestionService;