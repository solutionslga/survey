const express = require('express')
const QuestionController = require('../controllers/QuestionController');
const questionModel = require('../models/QuestionModel');
const QuestionService = require('../services/QuestionService');
 
const router = express.Router();

const questionservice = new QuestionService(questionModel);
const questionController = new QuestionController(questionservice);

router.post('/create',questionController.createQuestion.bind(questionController));
router.delete('/:questionId',questionController.deleteQuestionById.bind(questionController));
router.get('/:questionId',questionController.getQuestionById.bind(questionController));
router.get('',questionController.getAllQuestions.bind(questionController));
 
module.exports = router;