class QuestionController {
 
    constructor(questionService){
        this._questionService = questionService; 
    }

    async createQuestion(req,res,next){
        !Object.keys(req.body).length ?
            req.body = {
                message:'data not found'
            }:
            await this._questionService.createQuestion(req.body);
            return res.send(req.body);
    }
    
    async getAllQuestions(req,res,next){
        const questions =  await this._questionService.getAllQuestions();
        return res.send(questions);  
    }

    async getQuestionById(req,res,next){
        const questionId = req.params.questionId;
        const question = await this._questionService.getQuestionById(questionId);
        return res.send(question);  
    }
    async deleteQuestionById(req,res,next){
        const questionId = req.params.questionId;
        const question = await this._questionService.deleteQuestionById(questionId);
        return res.send(question);  
    }

}

module.exports = QuestionController;