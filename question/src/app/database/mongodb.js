const mongoose  = require('mongoose');
const mongodb = require('../../config/app').mongodb;

const endpoint = mongodb.dbHost;
const dbport = mongodb.dbport;
const dbName = mongodb.dbName;
const url = `mongodb://${endpoint}:${dbport}/${dbName}`;
const options =  {
    useNewUrlParser: true, 
    useCreateIndex: true, 
    useUnifiedTopology: true
};

mongoose.connect(url,options).then(
    () => {
        console.log("success connected");
    },
    err => {
        console.log(err.toString()) 
    }
  );
  module.exports = mongoose;


