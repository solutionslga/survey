const dbConn = require('../database/mongodb');

const questions = new dbConn.Schema({
    questionId: {
        type: Number,
    },
    questionCategoryName: {
        type: String,
    },
    questionDescription: {
        type: String,
    },
    questionAttempts: {
        type: Array,
    },
    correct: {
        type: String,
    },
    status: {
        type: Boolean,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

module.exports = dbConn.model('Questions', questions);