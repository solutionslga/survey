Este microapp tem a responsabilidade de inserir questões em um banco não relacional.
No caso usamos o mongoDB.
Exemplo de json para gravar uma questão no banco.

{
    "questionId": "1",
	"questionCategoryName": "Biologia",
	"questionDescription": "O tema teoria da evolução tem provocado debates em certos locais dos Estados Unidos da América, com algumas entidades contestando seu ensino nas escolas. Nos últimos tempos, a polêmica está centrada no termo teoria que, no entanto, tem significado bem definido para os cientistas.Sob o ponto de vista da ciência, teoria é: ",
	"questionAttempts": {
		"1": "Sinônimo de lei científica, que descreve regularidades de fenômenos naturais,mas não permite fazer previsões sobre eles.",
		"2": "Sinônimo de hipótese, ou seja, uma suposição ainda com comprovação experimental não permite fazer previsões sobre eles.",
		"3": "Uma ideia sem base em observação e experimentação, que usa o senso comum para explicar fatos do cotidiano.",
		"4": "Uma ideia, apoiada no conhecimento científico, que tenta explicar fenômenos naturais relacionados,permitindo fazer previsões",
		"5": "Uma ideia, apoiada pelo conhecimento científico, que, de tão comprovada pelos cientistas,já é considerada uma verdade incontestável."
	},
	"correct": "1"
}

Esta api permite a busca de todas as questoes ou por questionId e permite a criação de remoção das mesmas.

exemplo do arquivo .env

DB_HOST=localhost
DB_NAME=test
DB_PORT=27017








